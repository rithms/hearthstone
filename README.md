Hearthstone API and JSON data
===========

Download
--------

The latest build can be found on the [releases page](https://github.com/rithms/hearthstone-api/releases).

Dependencies
------------

- [Python 2.7](https://www.python.org/download/releases/2.7/)

- [DisUnity](https://github.com/ata4/disunity)

- [Beautiful Soup 4](http://www.crummy.com/software/BeautifulSoup/bs4/doc/)

Usage
-----

To extract the XML data from the client, you can use [DisUnity](https://github.com/ata4/disunity)

    $ disunity extract cardxml0.unity3d

After extracting the XML data using disunity, you can start the conversion by running the xml_to_json.py script

    $ python xml_to_json.py


Example JSON Data
-----------------

```json
{ "EX1_116": {
      "id": "EX1_116",
      "cardName": "Leeroy Jenkins",
      "cardSet": "Classic",
      "cardType": "Minion",
      "faction": "Alliance",
      "rarity": "Legendary",
      "attack": "6",
      "health": "2",
      "cost": "5",
      "cardTextInHand": "<b>Charge</b>. <b>Battlecry:</b> Summon two 1/1 Whelps for your opponent.",
      "flavorText": "At least he has Angry Chicken.",
      "artistName": "Gabe from Penny Arcade",
      "mechanics": [
        "Battlecry",
        "Charge"
      ],
      "collectible": true,
      "elite": true,
      "attackVisualType": "6",
      "enchantmentBirthVisual": "0",
      "enchantmentIdleVisual": "0"
    }
}
```

